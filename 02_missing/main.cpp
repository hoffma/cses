#include <iostream>

using namespace std;

int main() {
    size_t n = 0;
    size_t missing_sum = 0;
    cin >> n;
    size_t sum = (n*(n+1))/2;

    for(size_t i = 1; i < n; i++) {
        size_t tmp;
        cin >> tmp;
        missing_sum += tmp;
    }

    cout << (sum-missing_sum) << endl;


    return 0;
}
