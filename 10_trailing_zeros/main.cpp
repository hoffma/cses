#include <iostream>
#include <stdint.h>

using namespace std;

uint64_t trailing_zeros(uint64_t n) {
    // constraints: 1 ≤ n ≤ 10e9
    // no need to check for n < 0

    uint64_t cnt = 0;
    /* it's enough to check the powers of 5, because numbers of 2s >= 5s in
     * prime factors */
    for(uint64_t i = 5; n/i >= 1; i *= 5) {
        cnt += n/i;
    }

    return cnt;
}


int main() {

    uint64_t n = 0;
    cin >> n;

    cout << trailing_zeros(n) << endl;

    return 0;
}
