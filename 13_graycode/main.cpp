#include <iostream>
#include <sstream>
#include <iomanip>      // std::setfill, std::setw
#include <cmath>

using namespace std;

string int2bin(uint64_t n) {
    stringstream ss;
    if(n == 0) return "0";

    while(n > 0) {
        char tmp = '0' + (n&0x1);
        ss << tmp;
        n >>= 1;
    }

    string res = ss.str();

    return res.assign(res.rbegin(), res.rend());
}

uint64_t gray(uint64_t n) {
    return (n ^ (n/2));
}

int main() {
    uint64_t n;
    cin >> n;


    for(auto i = 0; i < pow(2, n); i++)
        cout << std::setw(n) << std::setfill('0') << int2bin(gray(i)) << endl;

    return 0;
}
