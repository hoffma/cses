#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int64_t MAX_NM = 2e5;

int64_t applicants[MAX_NM];
int64_t apartments[MAX_NM];


int main() {
    int64_t n, m, k;

    cin >> n >> m >> k;
    for(auto i = 0; i < n; ++i) cin >> applicants[i];
    for(auto i = 0; i < m; ++i) cin >> apartments[i];

    sort(applicants, applicants + n);
    sort(apartments, apartments + m);

    int64_t i = 0;
    int64_t j = 0;
    int64_t cnt = 0;
    while(i < n && j < m) {
        if(apartments[j] < (applicants[i] - k)) {
            j++; // apartment too small, look for the next one
        } else if((apartments[j] >= (applicants[i]-k)) && (apartments[j] <= applicants[i]+k)) {
            cnt++; i++; j++; // found a fitting apartment
        } else if(apartments[j] > (applicants[i] + k)) {
            i++; // apartment is too big for current applicant
        }
    }

    cout << cnt << endl;

    return 0;
}
