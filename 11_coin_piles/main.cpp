#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <sstream>
#include <algorithm> // swap

using namespace std;

struct Pile {
    uint64_t a, b;
    string res;

    string check_pile() {
        bool doable = true;

        /* check if only one is zero */
        doable &= not (a == 0 && b > 0);
        /* check if sum is dividable by 3 */
        doable &= ((a+b)%3 == 0);
        /* it's not working if one is more than twice the size */
        doable &= not (b > 2*a);

        return doable ? "YES" : "NO";
    };
};

void process_test(const string &filename) {
    fstream newfile;

    newfile.open(filename,ios::in); //open a file to perform read operation using file object
    if (newfile.is_open()){   //checking whether the file is open
        string tp;
        while(getline(newfile, tp)){ //read data from file object and put it into string.
            Pile tmp;
            istringstream iss(tp);
            iss >> tmp.a >> tmp.b >> tmp.res;
            if(tmp.a > tmp.b) swap(tmp.a, tmp.b); // always have a <= b
            
            string out = tmp.check_pile();
            cout << "a: " << tmp.a << ", b: " << tmp.b << ", res: " << tmp.res << ", out: " << out << endl;
            assert(out == tmp.res);
        }
        newfile.close(); //close the file object.
    }
}

int main() {

#if 1
    uint64_t t = 0;
    vector<Pile> piles;

    cin >> t;
    for(uint64_t i = 0; i < t; i++) {
        Pile tmp;
        cin >> tmp.a >> tmp.b;
        if(tmp.a > tmp.b) swap(tmp.a, tmp.b); // always have a <= b
        piles.push_back(tmp);
    }

    for(auto &p : piles) {
        cout << p.check_pile() << endl;
    }
#else
    process_test("test0.txt");
    process_test("test1.txt");
    process_test("test2.txt");
#endif

    return 0;
}
