#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
vector<string> permutations;

bool check(string s, int start, int curr) {
    for(int i = start; i < curr; ++i) {
        if(s[i] == s[curr]) return false;
    }

    return true;
}

void permute(string s, int idx, int n) {
    if(idx >= n) {
        permutations.push_back(s);
    }
    else {
        for(auto i = idx; i < n; ++i) {
            bool c = check(s, idx, i);
            if(c) {
                swap(s[idx], s[i]);
                permute(s, idx+1, n);
                swap(s[idx], s[i]);
            }
        }
    }
}

int main() {

    string inp;
    cin >> inp;

    permute(inp, 0, inp.size());
    std::sort(permutations.begin(), permutations.end());
    cout << permutations.size() << endl;
    for(auto &s : permutations) {
        cout << s << endl;
    }

    return 0;
}
