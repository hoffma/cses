#include <iostream>
#include <array>
#include <algorithm>

using namespace std;

int main() {
    string str;
    size_t cnt = 0;
    size_t max = 0;
    char prev = 'Z';
    cin >> str;

    for(auto c : str) {
        if(prev != c) {
            prev = c;
            cnt = 1;
        } else {
            cnt++;
        }
        max = cnt > max ? cnt : max;
    }

    cout << max << endl;

    return 0;
}
