#include <iostream>

using namespace std;

int main() {

    size_t n = 0;
    cin >> n;

    if(n == 2 || n == 3) {
        cout << "NO SOLUTION" << endl;
    } else {
        for(size_t i = 1; i <= n; i++) {
            if(i % 2 == 0)
                cout << i << " ";
        }
        for(size_t i = 1; i <= n; i++) {
            if(i % 2 == 1)
                cout << i << " ";
        }
        cout << endl;
    }

    return 0;
}

/*
 * 1 2 3
 * 
 * 2 1 3
 *
 *
 * 1 2 3 4
 * 2 4 1 3
 *
 * 1 2 3 4 5
 * 2 4 1 3 5
 *
 * 1 2 3 4 5 6
 * 2 4 6 1 3 5
*/
