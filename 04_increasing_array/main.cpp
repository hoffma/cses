#include <iostream>
#include <vector>

using namespace std;

int main() {

    size_t n = 0;
    size_t prev = 0;
    size_t steps = 0;
    vector<size_t> nums;
    cin >> n;

    for(size_t i = 0; i < n; i++) {
        size_t tmp;
        cin >> tmp;
        nums.push_back(tmp);
    }

    for(size_t i = 0; i < nums.size(); i++) {
        if(i == 0) {
            prev = nums[i];
        } else {
            if(prev > nums[i]) {
                size_t tmp = (prev-nums[i]);
                nums[i] += tmp;
                steps += tmp;
                continue;
            }
            prev = nums[i];
        }
    }

    cout << steps << endl;

    return 0;
}
