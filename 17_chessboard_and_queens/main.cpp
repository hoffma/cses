#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

#define FREE 0
#define TAKEN 1
#define RESERVED 2
#define ATTACKED 3

uint8_t chessboard[8][8] = {0};

void print_board() {
    for(auto i = 0; i < 8; i++) {
        for(auto j = 0; j < 8; j++) {
            cout << (int)chessboard[i][j] << " ";
        }
        cout << '\n';
    }
}

void place(uint8_t row, uint8_t col) {
    chessboard[row][col] = TAKEN;

    for(auto i = 0; i < 8; i++) {
        for(auto j = 0; j < 8; j++) {
            if((i == row) && (j == col)) continue;
        }
    }

}

int main() {

    for(auto i = 0; i < 8; i++) {
        for(auto j = 0; j < 8; j++) {
            char c;
            cin >> c;
            chessboard[i][j] = (c == '*') ? RESERVED : FREE;
        }
    }

    print_board();

    return 0;
}
