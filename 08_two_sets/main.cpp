#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

uint64_t sumSet(const vector<uint64_t> &s) {
    uint64_t sum = 0;

    for(const auto &n : s) {
        sum += n;
    }

    return sum;
}

int main() {

    uint64_t n = 0;
    cin >> n;

    vector<uint64_t> set1, set2;

    uint64_t sum = n*(n+1)/2;

    if(sum%2 == 1) {
        cout << "NO" << endl;
        return 0;
    }


    sum /= 2;
    for(auto i = n; i > 0; i--) {
        /* add numbers to first set til we get close to sum/2,
         * then add the rest */
        if(sum >= i) {
            sum -= i;
            set1.push_back(i);
        } else {
            set2.push_back(i);
        }
    }

    cout << "YES" << endl;
    cout << set1.size() << endl;
    for(const auto &n : set1) {
        cout  << n << " ";
    }
    cout << "\n";
    cout << set2.size() << endl;
    for(const auto &n : set2) {
        cout  << n << " ";
    }
    cout << "\n";

    return 0;
}
