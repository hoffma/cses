#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <sstream>

using namespace std;

void distinct_numbers() {
    uint64_t n;
    uint64_t tmp_num;
    uint64_t cnt;
    unordered_map<string, uint64_t> nums;
    string inp, val;
    stringstream ss;
    
    cin >> n;
    getline(cin >> ws, inp);
    
    ss << inp;
    while(!ss.eof()) {
        // ss >> tmp_num;
        ss >> val;
        nums[val] = 1;
    }
    
    cout << nums.size() << endl;

}


int main() {

    distinct_numbers();

    return 0;
}
