#include <iostream>
#include <vector>
#include <assert.h>
#include <cmath>

using namespace std;


int main() {

    uint64_t n = 0;
    cin >> n;

    for(uint64_t i = 1; i <= n; i++) {
        uint64_t knight1 = i*i;
        uint64_t knight2 = knight1-1;
        // ways to place the two knights (both are identical)
        uint64_t wtp = (knight1*knight2)/2; 
        /* We can have n-1 blocks horizontal, because they each overlap by one
         * block and the block has a width of 2. Same goes for vertical block.
         * Height is 3 and they each overlap as much as possible.*/
        uint64_t blocks = 2*(i-1)*(i-2); // number of 2x3 and 3x2 blocks
        uint64_t attack_ways = 2*blocks; // two attacks per block possible

        uint64_t res = i < 2 ? 0 : (wtp-attack_ways);
        
        cout << res << endl;
    }

    return 0;
}
