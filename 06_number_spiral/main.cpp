#include <iostream>
#include <vector>
#include <assert.h>
#include <cmath>

using namespace std;

struct Coord {
    size_t y, x;

    friend ostream& operator<<(ostream& os, const Coord& c);

};

ostream& operator <<(ostream &os, const Coord& c) {
    os << "{" << c.y << ", " << c.x << "}";
    return os;
}

size_t get_spiral_number(const Coord &c) {
    size_t a = c.y;
    size_t b = c.x;
    size_t erg = 0;

    if(a > b) {
        if(a%2 == 0) 
            erg = (a*a - b +1);
        else 
            erg = ((a-1)*(a-1) + b);
    } else {
        if(b%2 == 0) 
            erg = ((b-1)*(b-1) + a);
        else 
            erg = (b*b - a+1);
    }

    return erg;
}

int main() {

    size_t tests = 0;
    cin >> tests;
    vector<Coord> coords;

    for(size_t i = 0; i < tests; i++) {
        Coord tmp;
        cin >> tmp.y >> tmp.x;
        coords.push_back(tmp);
    }

    for(const auto &c : coords) {
        cout << get_spiral_number(c) << endl;
    }

    return 0;
}
