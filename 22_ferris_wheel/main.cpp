#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int64_t MAX_CHILDREN = 2e5;

int64_t children[MAX_CHILDREN];


int main() {
    int64_t n, x;
    int64_t cnt;

    cin >> n >> x;
    for(auto i = 0; i < n; ++i) cin >> children[i];

    for(auto i = 0; i < n; i++) {
        if(children[i] == -1) continue;
        int64_t sum = children[i]; // put first child into the gondola
        for(auto j = i+1; j < n; j++) {
            if(children[j] == -1) continue;
            int64_t tmp = sum + children[j];
            int64_t max = 0;
            if(tmp == x) { // max weight. Put two children into a gondola
                cnt++;
                children[j] = -1;
                break; // found perfect pair
            } else if(tmp < x && tmp > max) {
            }
        }
    }

    return 0;
}
