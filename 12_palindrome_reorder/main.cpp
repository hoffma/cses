#include <iostream>
#include <sstream>

using namespace std;

string reorder(string s) {
    stringstream palindrome;
    int cnt[26];
    bool uneven = false;

    for(int i = 0; i < 26; i++) cnt[i] = 0;

    for(auto &c : s) cnt[c-'A']++;

    for(int i = 0; i < 26; i++) {
        if(cnt[i]%2 == 1) {
            if(s.size()%2 == 0 || uneven == true) {
                return "NO SOLUTION";
            }
            uneven = true;
        }
    }
    if(s.size()%2 == 1 && uneven == false) {
        return "NO SOLUTION";
    }

    /* first pass */
    char tmp = 0;
    for(int i = 0; i < 26; i++) {
        if(cnt[i] > 0) {
            if(cnt[i]%2 == 1) {
                tmp = i+'A';
                cnt[i]--;
            }
            for(int j = 0; j < cnt[i]/2; j++) {
                char c = i + 'A';
                palindrome << c;
            }
        }
    }
    if(uneven) palindrome << tmp;
    /* second pass */
    for(int i = 25; i >= 0; i--) {
        if(cnt[i] > 0) {
            for(int j = 0; j < cnt[i]/2; j++) {
                char c = i + 'A';
                palindrome << c;
            }
        }
    }

    return palindrome.str(); 
}

void check_tests() {
}

int main() {
    string in;
    cin >> in;

    /*cout << reorder("AAAACACBA") << endl;*/
    cout << reorder(in) << endl;

    return 0;
}
