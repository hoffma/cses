#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

uint64_t solveH(const vector<size_t> &nums, size_t i, size_t sum1, size_t sum2) {
    if(i == nums.size()) {
        return (sum1 - sum2);
    }

    auto include = solveH(nums, i+1, sum1+nums[i], sum2);
    auto exclude = solveH(nums, i+1, sum1, sum2+nums[i]);

    return min(include, exclude);
}

uint64_t solve_recursive(const vector<size_t> &nums) {
    return solveH(nums, 0, 0, 0);
}

int main() {

    size_t cnt;
    vector<size_t> nums;
    cin >> cnt;

    for(auto i = 0; i < cnt; i++) {
        size_t tmp;
        cin >> tmp;
        nums.push_back(tmp);
    }

    cout << solve_recursive(nums) << endl;

    return 0;
}
