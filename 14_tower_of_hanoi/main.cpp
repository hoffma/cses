#include <deque>
#include <iostream>
#include <tuple>
#include <vector>

using namespace std;

struct tower {
  int id;
  deque<int> discs;
};

tower A = {1};
tower B = {2};
tower C = {3};

vector<tuple<int, int>> moves;

/**
 * Hello This is a discription
 *
 */
void print(std::deque<int> const &a) {
  for (size_t i = 0; i < a.size(); i++)
    std::cout << a.at(i) << ' ';
  cout << "\n";
}

void print_all() {
  cout << "#####################################\n";
  cout << "A: ";
  print(A.discs);
  cout << "B: ";
  print(B.discs);
  cout << "C: ";
  print(C.discs);
  cout << "#####################################\n";
}

void move(int n, tower &src, tower &dst, tower &aux) {
  if (n > 0) {
    move(n - 1, src, aux, dst);
    dst.discs.push_front(src.discs.front());
    src.discs.pop_front();
    /*print_all();*/
    moves.push_back(make_tuple(src.id, dst.id));
    /*cout << src.id << " " << dst.id << endl;*/
    move(n - 1, aux, dst, src);
  }
}

int main() {

  uint64_t n;
  cin >> n;

  for (uint64_t i = 1; i <= n; i++) {
    A.discs.push_back(i);
  }

  move(n, A, C, B);

  cout << moves.size() << endl;
  for (auto &m : moves) {
    cout << std::get<0>(m) << " " << std::get<1>(m) << endl;
  }

  return 0;
}
