
def modular_pow(e):
    b = int(2)
    m = 10e9+7

    c = 1
    ep = 0

    while ep < e:
        ep += 1
        c = int((b*c)%m)

    return c

print(modular_pow(27))

