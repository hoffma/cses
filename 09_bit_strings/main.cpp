#include <iostream>
#include <stdint.h>

using namespace std;

constexpr int64_t m = 1000000007; // 10e9+7

int64_t modpow(int64_t e) {
    const int64_t b = 2;

    int64_t c = 1;
    int64_t ep = 0;

    while(ep < e) {
        ep++;
        c = (b*c)%m;
    }
    
    return c;
}

int main() {

    double n = 0;
    cin >> n;

    cout << modpow(n) << endl;

    return 0;
}
