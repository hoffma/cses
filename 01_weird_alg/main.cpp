#include <iostream>

using namespace std;

int main() {

    size_t n;
    cin >> n;
    
    auto collatz = [](size_t x) { return (x%2 ? (x*3 + 1) : (x/2)); };
    while(n > 1) {
        cout << n << " ";
        n = collatz(n);
    }
    cout << n << std::endl;

    return 0;
}
